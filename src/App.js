import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./page/Home";
import Register from "./component/Register";
import Login1 from "./component/Login1";
import Sidebar from "./component/Sidebar";
import Dasboard from "./page/Dasboard";
import PeriksaPasien from "./page/PeriksaPasien";
import DaftarGuru from "./page/data/DaftarGuru";
import DaftarSiswa from "./page/data/DaftarSiswa";
import DaftarKaryawan from "./page/data/DaftarKaryawan";
import Diagnosa from "./page/Diagnosa";
import Penanganan from "./page/Penanganan";
import Tindakan from "./page/Tindakan";
import DaftarObat from "./page/DaftarObat";
import Profile from "./component/Profile";
import Profile1 from "./component/Profile1";
import StatusPriksa from "./page/data/StatusPriksa";
import EditDiagnosa from "./edit/EditDiagnosa";
import EditPenangananPertama from "./edit/EditPenanganan";
import EditTindakan from "./edit/EditTindakan";
import EditObat from "./edit/EditObat";
import EditGuru from "./edit/EditGuru";
import EditSiswa from "./edit/EditSiswa";
import EditKaryawan from "./edit/EditKaryawan";

function App() {
  return (
    <div>
      <BrowserRouter>
        <main>
          <Switch>
          <Route path="/" component={Login1} exact />
          <Route path="/register" component={Register} exact />
          <Route path="/sidebar" component={Sidebar} exact />
          <Route path="/profil" component={Profile} exact />
          <Route path="/prof" component={Profile1} exact />
          <Route path="/home" component={Home} exact />
          <Route path="/dasboard" component={Dasboard} exact />
          <Route path="/periksa" component={PeriksaPasien} exact />
          <Route path="/daftarGuru" component={DaftarGuru} exact />
          <Route path="/daftarSiswa" component={DaftarSiswa} exact />
          <Route path="/daftarKaryawan" component={DaftarKaryawan} exact />
          <Route path="/diagnosa" component={Diagnosa} exact />
          <Route path="/penanganan" component={Penanganan} exact />
          <Route path="/tindakan" component={Tindakan} exact />
          <Route path="/daftarObat" component={DaftarObat} exact />

          <Route path="/statusPriksa" component={StatusPriksa} exact />
          <Route path="/editDiagnosa/:id" component={EditDiagnosa} exact />
          <Route path="/editPenanganan/:id" component={EditPenangananPertama} exact />
          <Route path="/editTindakan/:id" component={EditTindakan} exact />
          <Route path="/editObat/:id" component={EditObat} exact />
          <Route path="/editGuru/:id" component={EditGuru} exact />
          <Route path="/editSiswa/:id" component={EditSiswa} exact />
          <Route path="/editKaryawan/:id" component={EditKaryawan} exact />
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
