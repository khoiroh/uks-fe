import React from "react";
import Sidebar from "./Sidebar";
import Navbar from "./Navbar";

export default function () {
  return (
    <div>
      <>
        <Navbar />
        <div className="flex">
          <Sidebar />
          <h1 className="pl-64 w-full">
            <h2 className="p-5">
              <div className="shadow-md">
                <p className="text-center p-2 bg-green-600 rounded-t-md text-white text-xl">
                  Khoir
                </p>
                <h1 className="flex rounded-b-md">
                  <p>
                    <img
                      src="https://ps.w.org/simple-user-avatar/assets/icon-256x256.png?rev=2413146"
                      alt=""
                      width={300}
                      className="h-[18rem]"
                    />
                  </p>
                  <div className="flex gap-2 p-3 text-lg">
                    <h2 className="font-medium">
                      <p>Email</p>
                      <p>Password</p>
                    </h2>
                    <h2 className="font-medium ">
                      <p>: khoir@gmail.com</p>
                      <p>: *********</p>
                      {/* <p className="mt-5">
                        Kelola informasi profil Anda untuk mengontrol,
                        melindungi dan mengamankan akun
                      </p> */}
                      <p className="mt-[10rem]">
                        <button type="button" className="btn btn-primary bg-green-600 w-[15rem]">Edit</button>
                        <button type="button" className="btn btn-primary bg-green-600 ml-3 w-[15rem]">Edit Password</button>
                      </p>
                    </h2>
                  </div>
                </h1>
              </div>
            </h2>
          </h1>
        </div>
      </>
    </div>
  );
}
