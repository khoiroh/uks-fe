import React from "react";
import Sidebar from "../component/Sidebar";
import Navbar from "../component/Navbar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";
import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";

export default function () {
  const [tindakan, setTindakan] = useState([]);
  const [namaTindakan, setNamaTindakan] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAll = async () => {
    await axios
      .get(`http://localhost:2099/tindakan`)
      .then((res) => {
        setTindakan(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post("http://localhost:2099/tindakan", {
        namaTindakan: namaTindakan,
      });
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteTindakan = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2099/tindakan/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />
        <h1 className="pl-64 w-full">
          <div className="p-5">
            <div className="shadow mt-4">
              <h1 className="flex bg-green-600 p-3 rounded-t">
                <p className="mt-1 text-white text-xl">Daftar Tindakan</p>
                <button
                  className="btn btn-primary ml-auto bg-green-900 w-[15rem]"
                  type="button"
                  onClick={handleShow}
                >
                  Tambah
                </button>
              </h1>
              {/* table */}
              <h1 className="p-3 rounded-b">
                <div class="flex flex-col">
                  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                      <div class="overflow-hidden">
                        <table class="min-w-full text-center text-sm font-light">
                          <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                            <tr>
                              <th scope="col" class="px-6 py-3">
                                No.
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Nama Tindakan
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Aksi
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {tindakan.map((tindakans, index) => {
                              return (
                                <tr
                                  class="border-b text-base bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700"
                                  key={index.id}
                                >
                                  <td class="whitespace-nowrap px-6 py-3 font-medium">
                                    {index + 1}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {tindakans.namaTindakan}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    <button class="bg-green-600 hover:bg-green-700 font-bold py-1 px-2 rounded">
                                      <a href={"/editTindakan/" + tindakans.id}>
                                        <i class="fa-solid fa-pen-to-square text-white"></i>
                                      </a>
                                    </button>
                                    <button
                                      onClick={() =>
                                        deleteTindakan(tindakans.id)
                                      }
                                      class="bg-red-600 ml-2 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                    >
                                      <i class="fa-solid fa-trash-can"></i>
                                    </button>
                                  </td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </h1>
            </div>
          </div>
        </h1>
      </div>
      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Tindakan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={add}>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Tindakan
              </label>
              <input
                placeholder="nama Tindakan"
                value={namaTindakan}
                onChange={(e) => setNamaTindakan(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <Modal.Footer>
              <Button
                variant="danger"
                className="bg-red-600"
                onClick={handleClose}
              >
                Batal
              </Button>
              <Button variant="primary" className="bg-cyan-600" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
