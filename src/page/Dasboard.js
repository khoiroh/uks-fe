import React from "react";
import Sidebar from "../component/Sidebar";
import Navbar from "../component/Navbar";
import { useState, useEffect } from "react";
import axios from "axios";

export default function () {
  const [guru, setGuru] = useState([]);
  const [karyawan, setKaryawan] = useState([]);
  const [siswa, setSiswa] = useState([]);
  const getAllGuru = async () => {
    await axios
      .get(`http://localhost:2099/guru/all-guru`)
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllKaryawan = async () => {
    await axios
      .get(`http://localhost:2099/karyawan`)
      .then((res) => {
        setKaryawan(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllSiswa = async () => {
    await axios
      .get(`http://localhost:2099/siswa`)
      .then((res) => {
        setSiswa(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAllGuru();
    getAllKaryawan();
    getAllSiswa();
  }, []);
  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className="pl-64 w-full">
          <div className=" px-8">
            {/* dasbrd */}
            <div className="md:flex md:flex-row gap-2">
              {/* ke-1 */}
              <div className="mt-8 shadow rounded-t-md ml-[1px] md:ml-2 bg-green-600 text-white w-full md:w-[25rem] md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-5">
                    <i className="fas fa-wheelchair text-6xl mt-5 text-green-700 hover:scale-110 duration-300"></i>
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Guru
                      </p>
                      <div>
                        <p className="text-lg font-bold md:text-2xl pt-2">
                          {" "}
                          {guru.length} Guru
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="/daftarGuru">
                  <p className="mt-3 md:mt-10 text-center h-6 bg-green-700 hover:bg-green-800 hover:text-white duration-200 text-sm md:text-sm">
                    Info lebih lanjut
                    <i className="fa-solid fa-arrow-right text-xs bg-white text-green-700 px-[0.5%] py-[0.3%] rounded-full ml-1"></i>
                  </p>
                </a>
              </div>

              {/* ke-2 */}
              <div className="mt-8 shadow rounded-t-md ml-[1px] md:ml-2 bg-green-600 text-white w-full md:w-[25rem] md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-5">
                    <i className="fas fa-wheelchair text-6xl mt-5 text-green-700 hover:scale-110 duration-300"></i>
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Siswa
                      </p>
                      <div>
                        <p className="text-lg font-bold md:text-2xl pt-2">
                          {siswa.length} Siswa
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="/daftarSiswa">
                  <p className="mt-3 md:mt-10 text-center h-6 bg-green-700 hover:bg-green-800 hover:text-white duration-200 text-sm md:text-sm">
                    Info lebih lanjut
                    <i className="fa-solid fa-arrow-right text-xs bg-white text-green-700 px-[0.5%] py-[0.3%] rounded-full ml-1"></i>
                  </p>
                </a>
              </div>

              {/* ke-3 */}
              <div className="mt-8 shadow rounded-t-md ml-[1px] md:ml-2 bg-green-600 text-white w-full md:w-[25rem] md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-4">
                    <i className="fas fa-wheelchair text-6xl mt-5 text-green-700 hover:scale-110 duration-300"></i>
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Karyawan
                      </p>
                      <div>
                        <p className="text-lg font-bold md:text-2xl pt-2">
                          {karyawan.length} Karyawan
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="/daftarKaryawan">
                  <p className="mt-3 md:mt-10 text-center h-6 bg-green-700 hover:bg-green-800 hover:text-white duration-200 text-sm md:text-sm">
                    Info lebih lanjut
                    <i className="fa-solid fa-arrow-right text-xs bg-white text-green-700 px-[0.5%] py-[0.3%] rounded-full ml-1"></i>
                  </p>
                </a>
              </div>
            </div>

            {/* table */}
            <div className="mt-5 p-3">
              <div className="shadow">
                <div className="text-center bg-green-600 p-3 rounded-t-md text-white">
                  <h1 className="text-xl font-medium">Riwayat Pasien</h1>
                </div>
                <div class="flex flex-col px-3 rounded-b-md">
                  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                      <div class="overflow-hidden">
                        <table class="min-w-full text-center text-sm font-light">
                          <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                            <tr>
                              <th scope="col" class="px-6 py-3">
                                No.
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Nama Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Status Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Tanggal/Jam Periksa
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700">
                              <td class="whitespace-nowrap px-6 py-3 font-medium">
                                1
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">Mark</td>
                              <td class="whitespace-nowrap px-6 py-3">
                                Pusing
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                3 january
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
