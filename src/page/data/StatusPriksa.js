import React from "react";
import Sidebar from "../../component/Sidebar";

export default function () {
  return (
    <div>
      <Sidebar />
      <div className="pl-64 w-full">
        <div className="p-5">
          <h1 className="shadow">
            <h2 className="text-center bg-green-600 p-2 rounded-t-md text-white text-lg">
              Periksa Pasien : Cuaksss
            </h2>
            <h2>
              <form action="" className="p-3">
                <h1 className="flex gap-2">
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Nama Pasien
                    </label>
                    <input
                      placeholder="Nama Pasien"
                      className="bg-white md:mb-4 mb-3 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-[30rem] rounded h-9 md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    />
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Status Pasien
                    </label>
                    <input
                      placeholder="Status Pasien"
                      className="bg-white md:mb-4 mb-3 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-[30rem] rounded h-9 md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    />
                  </div>
                </h1>
                <h1>
                  <div>
                    <label className="block md:mb-3 mb-2 text-sm font-medium text-gray-900 dark:text-black">
                      Keluhan Pasien
                    </label>
                    <textarea
                      placeholder="Keluhan Pasien"
                      className="bg-white md:mb-4 mb-3 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block rounded w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                      required
                    ></textarea>
                  </div>
                </h1>
                <h1 className="flex gap-3">
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Penyakit Pasien
                    </label>
                    <select aria-label="Status" class="form-select w-[14rem]">
                      <option class="hidden">Pilih Penyakit</option>
                      <option value="Guru">kanker</option>
                      <option value="Siswa">penyakit hati</option>
                      <option value="Karyawan">mules</option>
                    </select>
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Penanganan Pertama
                    </label>
                    <select aria-label="Status" class="form-select w-[14rem]">
                      <option class="hidden">Pilih Penanganan</option>
                      <option value="Guru">di perban</option>
                      <option value="Siswa">di rantai</option>
                      <option value="Karyawan">di oprasi</option>
                    </select>
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Tindakan
                    </label>
                    <select aria-label="Status" class="form-select w-[14rem]">
                      <option class="hidden">Pilih Tindakan</option>
                      <option value="Guru">di bawa rs</option>
                      <option value="Siswa">di oprasi</option>
                      <option value="Karyawan">di suntik</option>
                    </select>
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Catatan
                    </label>
                    <input className="bg-white md:mb-4 mb-3 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-[14rem] rounded h-9 md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black" />
                    
                    <button
                      type="button"
                      className="btn btn-success w-[8rem] bg-green-600"
                    >
                      Simpan
                    </button>
                  </div>
                </h1>
                {/* table */}
                <div className="mt-3">
                  <div className="">
                    <div class="flex flex-col px-3 rounded-b-md">
                      <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                          <div class="overflow-hidden">
                            <table class="min-w-full text-center text-sm font-light">
                              <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                                <tr>
                                  <th scope="col" class="px-6 py-3">
                                    No.
                                  </th>
                                  <th scope="col" class="px-6 py-3">
                                    Penyakit Pasien
                                  </th>
                                  <th scope="col" class="px-6 py-3">
                                    Tindakan
                                  </th>
                                  <th scope="col" class="px-6 py-3">
                                    Catatan
                                  </th>
                                  <th scope="col" class="px-6 py-3">
                                    Aksi
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700">
                                  <td class="whitespace-nowrap px-6 py-3 font-medium">
                                    1
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    Mark
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    Pusing
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">-</td>
                                  <td class="whitespace-nowrap px-6 py-3">-</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </h2>
          </h1>
          <h1 className="shadow flex mt-3 p-3 rounded border">
            <p className="">
              <button className="btn btn-danger w-[8rem] bg-red-600">
                Kembali
              </button>
            </p>
            <p className="ml-[44rem]">
              <button className="btn btn-success w-[8rem] bg-green-600">
                Selesai
              </button>
            </p>
          </h1>
        </div>
      </div>
    </div>
  );
}
