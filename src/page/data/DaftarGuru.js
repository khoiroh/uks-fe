import React from "react";
import Sidebar from "../../component/Sidebar";
import Navbar from "../../component/Navbar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";
import { useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useEffect } from "react";

export default function () {
  const param = useParams();
  const [guru, setGuru] = useState([]);
  const [namaGuru, setNamaGuru] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAll = async () => {
    await axios
      .get(`http://localhost:2099/guru/all-guru`)
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post("http://localhost:2099/guru", {
        namaGuru: namaGuru,
        tempatLahir: tempatLahir,
        tanggalLahir: tanggalLahir,
        alamat: alamat,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteGuru = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2099/guru/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className="pl-64 w-full">
          <h1 className="p-5">
            <div className="shadow">
              <h1 className="flex gap-3 bg-green-600 p-3 rounded-t">
                <p className="mt-1 text-white text-xl">Daftar Guru</p>
                <button
                  className="btn btn-primary ml-auto bg-green-900 w-[15rem] h-8 text-sm"
                  type="button"
                  onClick={handleShow}
                >
                  Tambah
                </button>
                <p>
                  <button
                    type="button"
                    className="btn btn-primary bg-green-900 w-[15rem] h-8 text-sm"
                  >
                    Import Data
                  </button>
                  <p className="mt-2">
                    <button
                      type="button"
                      className="btn btn-primary bg-green-900 w-[15rem] h-8 text-sm"
                    >
                      Download Data
                    </button>
                  </p>
                </p>
              </h1>

              {/* table */}
              <h1 className="p-3 rounded-b">
                <div class="flex flex-col">
                  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                      <div class="overflow-hidden">
                        <table class="min-w-full text-center text-sm font-light">
                          <thead class="border-b uppercase bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                            <tr>
                              <th scope="col" class="px-6 py-3">
                                No.
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Nama Guru
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Tempat Tanggal Lahir
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Alamat
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Aksi
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {guru.map((gurus, index) => {
                              return (
                                <tr
                                  class="border-b text-base bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700"
                                  key={index.id}
                                >
                                  <td class="whitespace-nowrap px-6 py-3 font-medium">
                                    {index + 1}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {gurus.namaGuru}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {gurus.tempatLahir} ,{gurus.tanggalLahir}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {gurus.alamat}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    <button class="bg-green-600 hover:bg-green-700 font-bold py-1 px-2 rounded">
                                      <a href={"/editGuru/" + gurus.id}>
                                        <i class="fa-solid fa-pen-to-square  text-white"></i>
                                      </a>
                                    </button>
                                    <button
                                      onClick={() => deleteGuru(gurus.id)}
                                      class="bg-red-600 ml-2 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                    >
                                      <i class="fa-solid fa-trash-can"></i>
                                    </button>
                                  </td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </h1>
            </div>
          </h1>
        </div>
      </div>
      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Guru</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={add}>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Guru
              </label>
              <input
                placeholder="nama guru"
                value={namaGuru}
                onChange={(e) => setNamaGuru(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Tempat Lahir
            </label>
            <input
              placeholder="Tempat Lahir"
              value={tempatLahir}
              onChange={(e) => setTempatLahir(e.target.value)}
              className="bg-white border  mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Tanggal Lahir
            </label>
            <input
              placeholder="Tanggal Lahir"
              type="date"
              value={tanggalLahir}
              onChange={(e) => setTanggalLahir(e.target.value)}
              className="bg-white border mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Alamat
            </label>
            <input
              placeholder="Alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
              className="bg-white border md:mb-7 mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <Modal.Footer>
              <Button variant="danger" className="bg-red-600" onClick={handleClose}>
                Batal
              </Button>
              <Button variant="primary" className="bg-cyan-600" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
