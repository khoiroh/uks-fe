import React from "react";
import Navbar from "../component/Navbar";
import Sidebar from "../component/Sidebar";
import { useState } from "react";
import { Button, Modal } from "react-bootstrap";

export default function () {
  const [show1, setShow1] = useState(false);

  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className="w-full pl-64">
          <div className="p-5">
            {/* rekap data */}
            <div className="shadow">
              <h1 className="flex bg-green-600 p-3 rounded-t">
                <p className="mt-1 text-white text-xl">Filter Rekap Data</p>
                <button
                  className="btn btn-primary ml-auto bg-green-900 w-[18%]"
                  type="button"
                  onClick={handleShow1}
                >
                  Filter Tanggal
                </button>
              </h1>
              <div class="container flex flex-col items-center justify-center px-5 mx-auto my-8 space-y-8 text-center sm:max-w-md">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                  class="w-40 h-40 dark:text-gray-600"
                >
                  <path
                    fill="currentColor"
                    d="M256,16C123.452,16,16,123.452,16,256S123.452,496,256,496,496,388.548,496,256,388.548,16,256,16ZM403.078,403.078a207.253,207.253,0,1,1,44.589-66.125A207.332,207.332,0,0,1,403.078,403.078Z"
                  ></path>
                  <rect
                    width="176"
                    height="32"
                    x="168"
                    y="320"
                    fill="currentColor"
                  ></rect>
                  <polygon
                    fill="currentColor"
                    points="210.63 228.042 186.588 206.671 207.958 182.63 184.042 161.37 162.671 185.412 138.63 164.042 117.37 187.958 141.412 209.329 120.042 233.37 143.958 254.63 165.329 230.588 189.37 251.958 210.63 228.042"
                  ></polygon>
                  <polygon
                    fill="currentColor"
                    points="383.958 182.63 360.042 161.37 338.671 185.412 314.63 164.042 293.37 187.958 317.412 209.329 296.042 233.37 319.958 254.63 341.329 230.588 365.37 251.958 386.63 228.042 362.588 206.671 383.958 182.63"
                  ></polygon>
                </svg>
                <p class="text-3xl pb-5">
                  Filter terlebih dahulu sesuai tanggal yang diinginkan.
                </p>
              </div>
              {/* <h1 className="text-center p-4 rounded-b">
                <p className="text-4xl font-semibold">Rekap Data Pasien</p>
                <p className="mt-3 text-lg font-semibold">
                  (2023-05-05) - (2023-05-05)
                </p>
                <button
                  className="btn btn-primary bg-green-600 w-[30%] p-2 mt-2"
                  type="button"
                >
                  Download Rekap Data Pasien
                </button>
              </h1> */}
            </div>

            {/* table */}
            <div className="shadow mt-4">
              <h1 className="flex bg-green-600 p-3 rounded-t">
                <p className="mt-1 text-white text-xl">Daftar Pasien</p>
                <button
                  className="btn btn-primary ml-auto bg-green-900 w-[18%]"
                  type="button"
                  onClick={handleShow}
                >
                  Tambah
                </button>
              </h1>
              <h1 className="p-3 rounded-b">
                <div class="flex flex-col">
                  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                      <div class="overflow-hidden">
                        <table class="min-w-full text-center text-sm font-light">
                          <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                            <tr>
                              <th scope="col" class="px-6 py-3">
                                No.
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Nama Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Status Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Jabatan
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Tanggal/Jam
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Keterangan
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Status
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Aksi
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700">
                              <td class="whitespace-nowrap px-6 py-3 font-medium">
                                1
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">Mark</td>
                              <td class="whitespace-nowrap px-6 py-3">
                                Pusing
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                3 january
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">-</td>
                              <td class="whitespace-nowrap px-6 py-3">-</td>
                              <td class="whitespace-nowrap px-6 py-3">-</td>
                              <td class="whitespace-nowrap px-6 py-3">
                                <a href="/statusPriksa">
                                  <button
                                    className="btn btn-success bg-green-500"
                                    type="button"
                                  >
                                    Selesai
                                  </button>
                                </a>
                                <button
                                  className="btn btn-danger bg-red-500 ml-2"
                                  type="button"
                                >
                                  Tangani
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </h1>
            </div>
          </div>
        </div>
      </div>
      {/* Modal filter tambah pasien */}
      <Modal show={show1} onHide={handleClose1}>
        <Modal.Header closeButton>
          <Modal.Title>Filter Rekap Data Pasien</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div>
              <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
                Dari Tanggal :
              </label>
              <input
                placeholder="Tanggal Lahir"
                type="date"
                // value={tanggalLahir}
                // onChange={(e) => setTanggalLahir(e.target.value)}
                className="bg-white border mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <div>
              <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
                Sampai Tanggal :
              </label>
              <input
                placeholder="Tanggal Lahir"
                type="date"
                // value={tanggalLahir}
                // onChange={(e) => setTanggalLahir(e.target.value)}
                className="bg-white border mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              />
            </div>
            <Modal.Footer>
              <Button
                variant="danger"
                className="bg-red-600"
                onClick={handleClose1}
              >
                Batal
              </Button>
              <Button variant="primary" className="bg-cyan-600" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>

      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Daftar Pasien</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
              Status Pasien
            </label>

            <select aria-label="Status" class="form-select">
              <option class="hidden">Pilih Status</option>
              <option value="Guru">Guru</option>
              <option value="Siswa">Siswa</option>
              <option value="Karyawan">Karyawan</option>
            </select>

            <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
              Nama Pasien
            </label>
            <select aria-label="Status" class="form-select mt-3">
              <option class="hidden">Nama Pasien</option>
              <option value="Guru">Khoier</option>
              <option value="Siswa">luntel</option>
              <option value="Karyawan">nandol</option>
            </select>
            <div class="d-flex gap-3 mt-3 input-group">
              <input
                placeholder="Keluhan Pasien"
                class="form-control"
                value=""
              />
            </div>
            <Modal.Footer>
              <Button
                variant="danger"
                className="bg-red-600"
                onClick={handleClose}
              >
                Batal
              </Button>
              <Button variant="primary" className="bg-cyan-600" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
