import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function () {
  const [namaSiswa, setNamaSiswa] = useState("");
  const [kelas, setKelas] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [alamat, setAlamat] = useState("");

  const param = new useParams();
  const history = useHistory();

  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`http://localhost:2099/siswa/` + param.id, {
        namaSiswa: namaSiswa,
        kelas: kelas,
        tempatLahir: tempatLahir,
        tanggalLahir: tanggalLahir,
        alamat: alamat,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/daftarSiswa");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:2099/siswa/" + param.id)
      .then((response) => {
        const Siswa = response.data.data;
        setNamaSiswa(Siswa.namaSiswa);
        setKelas(Siswa.kelas);
        setTempatLahir(Siswa.tempatLahir);
        setTanggalLahir(Siswa.tanggalLahir);
        setAlamat(Siswa.alamat);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  return (
    <div>
      <div className="">
        <div className="px-[25rem] py-[2rem]">
          <form className="space-y-3 shadow p-3 w-full" onSubmit={Put}>
            <h3 className="md:py-4 py-3 md:text-2xl text-xl font-medium text-black dark:text-black">
              Edit Daftar Siswa
            </h3>
            <hr />
            <div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Nama Siswa
                </label>
                <input
                  placeholder="Nama Siswa"
                  value={namaSiswa}
                  onChange={(e) => setNamaSiswa(e.target.value)}
                  className="bg-gray-50 mb-4 border rounded w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Kelas
                </label>
                <input
                  placeholder="kelas"
                  value={kelas}
                  onChange={(e) => setKelas(e.target.value)}
                  className="bg-gray-50 mb-4 border rounded w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tempat Lahir
                </label>
                <input
                  placeholder="Tempat lahir"
                  value={tempatLahir}
                  onChange={(e) => setTempatLahir(e.target.value)}
                  className="bg-gray-50 mb-4 border rounded w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tanggal Lahir
                </label>
                <input
                  placeholder="0"
                  type="date"
                  value={tanggalLahir}
                  onChange={(e) => setTanggalLahir(e.target.value)}
                  className="bg-gray-50 mb-4 border rounded w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Alamat
                </label>
                <input
                  placeholder="alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  className="bg-gray-50 mb-4 border rounded w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div className="md:flex gap-[3rem]">
                <a
                  href="/daftarSiswa"
                  className="w-[7rem] rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm ml-auto md:py-2.5 py-1.5 text-center no-underline dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                >
                  Batal
                </a>
                <button
                  type="submit"
                  className="w-[7rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
